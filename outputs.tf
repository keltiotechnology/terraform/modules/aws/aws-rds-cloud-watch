output "alarm_free_storage_arns" {
  description = "List of ARNs for FreeStorageSpace alarms"
  value       = [for alarm in aws_cloudwatch_metric_alarm.disk_free_storage_space : alarm.arn]
}

output "alarm_cpu_utilization_arns" {
  description = "List of ARNs for CPUUtilization alarms"
  value       = [for alarm in aws_cloudwatch_metric_alarm.cpu_utilization : alarm.arn]
}

output "alarm_disk_queue_depth_arns" {
  description = "List of ARNs for DiskQueueDepth alarms"
  value       = [for alarm in aws_cloudwatch_metric_alarm.disk_queue_depth : alarm.arn]
}

output "alarm_freeable_memory_arns" {
  description = "List of ARNs for FreeableMemory alarms"
  value       = [for alarm in aws_cloudwatch_metric_alarm.freeable_memory : alarm.arn]
}

output "alarm_swap_usage_arns" {
  description = "List of ARNs for SwapUsage alarms"
  value       = [for alarm in aws_cloudwatch_metric_alarm.swap_usage : alarm.arn]
}

output "event_db_stopped_rule_arns" {
  description = "List of ARNs for DB Stopped event rules"
  value       = [for rule in aws_cloudwatch_event_rule.db_stopped : rule.arn]
}
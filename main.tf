/* ------------------------------------------------------------------------------------------------------------ */
/* Storage Space Alarm                                                                                        */
/* ------------------------------------------------------------------------------------------------------------ */
resource "aws_cloudwatch_metric_alarm" "disk_free_storage_space" {
  for_each            = var.monitor_free_storage_space ? toset(var.rds_instances_ids) : []
  alarm_name          = "${var.namespace}/${var.stage}/rds-${each.key}-lowFreeLocalStorage"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = var.free_storage_space.evaluation_periods
  metric_name         = "FreeLocalStorage"
  namespace           = "AWS/RDS"
  period              = var.free_storage_space.period
  statistic           = "Average"
  threshold           = var.free_storage_space.threshold
  alarm_description   = "Free storage space is too low for instance ${each.key}, and it may fill up soon."
  alarm_actions       = var.free_storage_space.alarm_actions

  dimensions = {
    DBInstanceIdentifier = each.key
  }
}

/* ------------------------------------------------------------------------------------------------------------ */
/* CPU Utilization Alarm                                                                                        */
/* ------------------------------------------------------------------------------------------------------------ */
resource "aws_cloudwatch_metric_alarm" "cpu_utilization" {
  for_each            = var.monitor_cpu_utilization ? toset(var.rds_instances_ids) : []
  alarm_name          = "${var.namespace}/${var.stage}/${var.cpu_utilization.alarm_name}-${each.key}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = var.cpu_utilization.evaluation_periods
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = var.cpu_utilization.period
  statistic           = "Average"
  threshold           = var.cpu_utilization.threshold
  alarm_description   = "Alert when RDS CPU utilization is too high for instance ${each.key}."
  alarm_actions       = var.cpu_utilization.alarm_actions

  dimensions = {
    DBInstanceIdentifier = each.key
  }
}

/* ------------------------------------------------------------------------------------------------------------ */
/* Disk Queue Depth Alarm                                                                                       */
/* ------------------------------------------------------------------------------------------------------------ */
resource "aws_cloudwatch_metric_alarm" "disk_queue_depth" {
  for_each            = var.monitor_disk_queue_depth ? toset(var.rds_instances_ids) : []
  alarm_name          = "${var.namespace}/${var.stage}/${var.disk_queue_depth.alarm_name}-${each.key}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = var.disk_queue_depth.evaluation_periods
  metric_name         = "DiskQueueDepth"
  namespace           = "AWS/RDS"
  period              = var.disk_queue_depth.period
  statistic           = "Average"
  threshold           = var.disk_queue_depth.threshold
  alarm_description   = "Alert when RDS disk queue depth is too high for instance ${each.key}."
  alarm_actions       = var.disk_queue_depth.alarm_actions

  dimensions = {
    DBInstanceIdentifier = each.key
  }
}

/* ------------------------------------------------------------------------------------------------------------ */
/* Freeable Memory Alarm                                                                                        */
/* ------------------------------------------------------------------------------------------------------------ */
resource "aws_cloudwatch_metric_alarm" "freeable_memory" {
  for_each            = var.monitor_freeable_memory ? toset(var.rds_instances_ids) : []
  alarm_name          = "${var.namespace}/${var.stage}/${var.freeable_memory.alarm_name}-${each.key}"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = var.freeable_memory.evaluation_periods
  metric_name         = "FreeableMemory"
  namespace           = "AWS/RDS"
  period              = var.freeable_memory.period
  statistic           = "Average"
  threshold           = var.freeable_memory.threshold
  alarm_description   = "Alert when RDS freeable memory is too low for instance ${each.key}."
  alarm_actions       = var.freeable_memory.alarm_actions

  dimensions = {
    DBInstanceIdentifier = each.key
  }
}

/* ------------------------------------------------------------------------------------------------------------ */
/* Swap Usage Alarm                                                                                             */
/* ------------------------------------------------------------------------------------------------------------ */
resource "aws_cloudwatch_metric_alarm" "swap_usage" {
  for_each            = var.monitor_swap_usage ? toset(var.rds_instances_ids) : []
  alarm_name          = "${var.namespace}/${var.stage}/${var.swap_usage.alarm_name}-${each.key}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = var.swap_usage.evaluation_periods
  metric_name         = "SwapUsage"
  namespace           = "AWS/RDS"
  period              = var.swap_usage.period
  statistic           = "Average"
  threshold           = var.swap_usage.threshold
  alarm_description   = "Alert when RDS swap usage is too high for instance ${each.key}."
  alarm_actions       = var.swap_usage.alarm_actions

  dimensions = {
    DBInstanceIdentifier = each.key
  }
}

/* ------------------------------------------------------------------------------------------------------------ */
/* Stopped Instances Event Rule                                                                                 */
/* ------------------------------------------------------------------------------------------------------------ */
resource "aws_cloudwatch_event_rule" "db_stopped" {
  for_each = toset(var.rds_instances_ids)

  name = "${var.namespace}-${var.stage}-DB-Stopped-${each.key}"
  event_pattern = jsonencode({
    "source" : ["aws.rds"],
    "detail-type" : ["RDS DB Instance Event"],
    "detail" : {
      "EventCategories" : [
        "notification", "availability"
      ],
      "SourceType" : ["DB_INSTANCE"],
      "SourceIdentifier" : [each.key],
      "EventID" : ["RDS-EVENT-0004", "RDS-EVENT-0006", "RDS-EVENT-0087"]
    }
  })
}

resource "aws_cloudwatch_event_target" "sns_topic" {
  for_each = aws_cloudwatch_event_rule.db_stopped

  rule      = aws_cloudwatch_event_rule.db_stopped[each.key].name
  target_id = "SendToSNS-${each.key}"
  arn       = var.stopped_event.alarm_actions[0]
}
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_event_rule.db_stopped](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_rule) | resource |
| [aws_cloudwatch_event_target.sns_topic](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_target) | resource |
| [aws_cloudwatch_metric_alarm.cpu_utilization](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.disk_free_storage_space](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.disk_queue_depth](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.freeable_memory](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.swap_usage](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cpu_utilization"></a> [cpu\_utilization](#input\_cpu\_utilization) | Configuration for CPU utilization alarm | <pre>object({<br/>    alarm_name         = string<br/>    evaluation_periods = number<br/>    period             = number<br/>    threshold          = number<br/>    alarm_actions      = list(string)<br/>  })</pre> | n/a | yes |
| <a name="input_disk_queue_depth"></a> [disk\_queue\_depth](#input\_disk\_queue\_depth) | Configuration for disk queue depth alarm | <pre>object({<br/>    alarm_name         = string<br/>    evaluation_periods = number<br/>    period             = number<br/>    threshold          = number<br/>    alarm_actions      = list(string)<br/>  })</pre> | n/a | yes |
| <a name="input_free_storage_space"></a> [free\_storage\_space](#input\_free\_storage\_space) | Configuration for free storage space alarms. | <pre>object({<br/>    evaluation_periods = number<br/>    period             = number<br/>    threshold          = number<br/>    alarm_actions      = list(string)<br/>  })</pre> | n/a | yes |
| <a name="input_freeable_memory"></a> [freeable\_memory](#input\_freeable\_memory) | Configuration for freeable memory alarm | <pre>object({<br/>    alarm_name         = string<br/>    evaluation_periods = number<br/>    period             = number<br/>    threshold          = number<br/>    alarm_actions      = list(string)<br/>  })</pre> | n/a | yes |
| <a name="input_monitor_cpu_utilization"></a> [monitor\_cpu\_utilization](#input\_monitor\_cpu\_utilization) | Enable CPU utilization monitoring. | `bool` | n/a | yes |
| <a name="input_monitor_disk_queue_depth"></a> [monitor\_disk\_queue\_depth](#input\_monitor\_disk\_queue\_depth) | Enable disk queue depth monitoring. | `bool` | n/a | yes |
| <a name="input_monitor_free_storage_space"></a> [monitor\_free\_storage\_space](#input\_monitor\_free\_storage\_space) | Enable or disable monitoring of free storage space. | `bool` | n/a | yes |
| <a name="input_monitor_freeable_memory"></a> [monitor\_freeable\_memory](#input\_monitor\_freeable\_memory) | Enable freeable memory monitoring. | `bool` | n/a | yes |
| <a name="input_monitor_swap_usage"></a> [monitor\_swap\_usage](#input\_monitor\_swap\_usage) | Enable swap usage monitoring. | `bool` | n/a | yes |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Namespace, which could be your organization name | `string` | n/a | yes |
| <a name="input_rds_instances_ids"></a> [rds\_instances\_ids](#input\_rds\_instances\_ids) | List of RDS instance identifiers | `list(string)` | n/a | yes |
| <a name="input_stage"></a> [stage](#input\_stage) | Stage, which can be 'prod', 'staging', 'dev'... | `string` | n/a | yes |
| <a name="input_stopped_event"></a> [stopped\_event](#input\_stopped\_event) | Configuration for DB Stopped events | <pre>object({<br/>    alarm_name         = string<br/>    alarm_actions      = list(string)<br/>  })</pre> | n/a | yes |
| <a name="input_swap_usage"></a> [swap\_usage](#input\_swap\_usage) | Configuration for swap usage alarm | <pre>object({<br/>    alarm_name         = string<br/>    evaluation_periods = number<br/>    period             = number<br/>    threshold          = number<br/>    alarm_actions      = list(string)<br/>  })</pre> | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_alarm_cpu_utilization_arns"></a> [alarm\_cpu\_utilization\_arns](#output\_alarm\_cpu\_utilization\_arns) | List of ARNs for CPUUtilization alarms |
| <a name="output_alarm_disk_queue_depth_arns"></a> [alarm\_disk\_queue\_depth\_arns](#output\_alarm\_disk\_queue\_depth\_arns) | List of ARNs for DiskQueueDepth alarms |
| <a name="output_alarm_free_storage_arns"></a> [alarm\_free\_storage\_arns](#output\_alarm\_free\_storage\_arns) | List of ARNs for FreeStorageSpace alarms |
| <a name="output_alarm_freeable_memory_arns"></a> [alarm\_freeable\_memory\_arns](#output\_alarm\_freeable\_memory\_arns) | List of ARNs for FreeableMemory alarms |
| <a name="output_alarm_swap_usage_arns"></a> [alarm\_swap\_usage\_arns](#output\_alarm\_swap\_usage\_arns) | List of ARNs for SwapUsage alarms |
| <a name="output_event_db_stopped_rule_arns"></a> [event\_db\_stopped\_rule\_arns](#output\_event\_db\_stopped\_rule\_arns) | List of ARNs for DB Stopped event rules |

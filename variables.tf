variable "namespace" {
  type        = string
  description = "Namespace, which could be your organization name"
}

variable "stage" {
  type        = string
  description = "Stage, which can be 'prod', 'staging', 'dev'..."
}

variable "rds_instances_ids" {
  type        = list(string)
  description = "List of RDS instance identifiers"
}

/* Storage Space Alarm */
variable "monitor_free_storage_space" {
  description = "Enable or disable monitoring of free storage space."
  type        = bool
}

variable "free_storage_space" {
  description = "Configuration for free storage space alarms."
  type = object({
    evaluation_periods = number
    period             = number
    threshold          = number
    alarm_actions      = list(string)
  })
}

/* CPU Utilization Alarm */
variable "monitor_cpu_utilization" {
  type        = bool
  description = "Enable CPU utilization monitoring."
}

variable "cpu_utilization" {
  type = object({
    alarm_name         = string
    evaluation_periods = number
    period             = number
    threshold          = number
    alarm_actions      = list(string)
  })
  description = "Configuration for CPU utilization alarm"
}

/* Disk Queue Depth Alarm */
variable "monitor_disk_queue_depth" {
  type        = bool
  description = "Enable disk queue depth monitoring."
}

variable "disk_queue_depth" {
  type = object({
    alarm_name         = string
    evaluation_periods = number
    period             = number
    threshold          = number
    alarm_actions      = list(string)
  })
  description = "Configuration for disk queue depth alarm"
}

/* Freeable Memory Alarm */
variable "monitor_freeable_memory" {
  type        = bool
  description = "Enable freeable memory monitoring."
}

variable "freeable_memory" {
  type = object({
    alarm_name         = string
    evaluation_periods = number
    period             = number
    threshold          = number
    alarm_actions      = list(string)
  })
  description = "Configuration for freeable memory alarm"
}

/* Swap Usage Alarm */
variable "monitor_swap_usage" {
  type        = bool
  description = "Enable swap usage monitoring."
}

variable "swap_usage" {
  type = object({
    alarm_name         = string
    evaluation_periods = number
    period             = number
    threshold          = number
    alarm_actions      = list(string)
  })
  description = "Configuration for swap usage alarm"
}

/* DB Stopped Event */
variable "stopped_event" {
  type = object({
    alarm_name    = string
    alarm_actions = list(string)
  })
  description = "Configuration for DB Stopped events"
}